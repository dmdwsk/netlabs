﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDelivery.Queries
{
    public class OrderQuery
    {
        public void GetItems(SqlConnection connection)
        {
            string sqlQuery = "Select * from Orders";

            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                Console.WriteLine("{0}\t{1,2}\t{2,10}\t{3,2}\t{4,3}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4));

                while (reader.Read())
                {
                    object Id = reader.GetValue(0);
                    object PizzaId = reader.GetValue(1);
                    object CLientId = reader.GetValue(2);
                    object DiscountId = reader.GetValue(3);
                    object time = reader.GetValue(4);

                    Console.WriteLine("{0} \t{1,1} \t{2,5} \t{3,10} \t{4,16}", Id, PizzaId, CLientId, DiscountId, time);
                }
            }
            reader.Close();
        }
    }
}
