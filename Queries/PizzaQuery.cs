﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDelivery.Queries
{
    public class PizzaQuery
    {
        public void GetItems(SqlConnection connection)
        {
            string sqlQuery = "Select * from Pizza";

            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                Console.WriteLine("{0}\t{1,2}\t{2,2}\t{3,2}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3));

                while (reader.Read())
                {
                    object Id = reader.GetValue(0);
                    object Price = reader.GetValue(1);
                    object OrderCount = reader.GetValue(2);
                    object Description = reader.GetValue(3);
                

                    Console.WriteLine("{0} \t{1,1} \t{2,1} \t{3,18} ", Id, Price, OrderCount, Description);
                }
            }
            reader.Close();
        }
    }
}
