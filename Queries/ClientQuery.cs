﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDelivery.Queries
{
    public class ClientQuery
    {
        public void GetItems(SqlConnection connection)
        {
            string sqlQuery = "Select * from Client";

            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                Console.WriteLine("{0}\t{1,2}\t{2,2}\t{3,2}\t{4,3}\t{5,14}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4), reader.GetName(5));

                while (reader.Read())
                {
                    object Id = reader.GetValue(0);
                    object FirstName = reader.GetValue(1);
                    object SecondName = reader.GetValue(2);
                    object ThirdName = reader.GetValue(3);
                    object Address = reader.GetValue(4);
                    object Phone = reader.GetValue(5);

                    Console.WriteLine("{0} \t{1,1} \t{2,15} \t{3,2} \t{4,18} \t{5,1}", Id, FirstName, SecondName, ThirdName, Address, Phone);
                }
            }
            reader.Close();
        }
    }
}
