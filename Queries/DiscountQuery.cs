﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDelivery.Queries
{
   public class DiscountQuery
   {
        public void GetItems(SqlConnection connection)
        {
            string sqlQuery = "Select * from Discount";

            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                Console.WriteLine("{0}\t{1,2}", reader.GetName(0), reader.GetName(1));

                while (reader.Read())
                {
                    object Id = reader.GetValue(0);
                    object Description = reader.GetValue(1);
              

                    Console.WriteLine("{0} \t{1,1}", Id, Description);
                }
            }
            reader.Close();
        }
    }
}
