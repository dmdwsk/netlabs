﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaDelivery.Queries;

namespace PizzaDelivery
{
    class Program
    {
        static void Main(string[] args)
        {
            ClientQuery client = new ClientQuery();
            DiscountQuery discount = new DiscountQuery();
            OrderQuery order = new OrderQuery();
            PizzaQuery pizza = new PizzaQuery();

            string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Database=PizzaDelivery;integrated security=True;MultipleActiveResultSets=True;";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                Console.WriteLine("Connected");

                Console.WriteLine("------Client-------");
                client.GetItems(connection);

                Console.WriteLine("\n-------Discount-----");
                discount.GetItems(connection);

                Console.WriteLine("\n-------Order-------");
                order.GetItems(connection);

                Console.WriteLine("\n----------Pizza--------");
                pizza.GetItems(connection);
            }

            Console.WriteLine("Disconnected");
            Console.Read();
        }
    }
}
